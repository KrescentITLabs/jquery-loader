(function() {
    'use strict';

    var string0 = $(".bigword").text();
    $(".bigword").text("");
    for (var n = 0; n < string0.length; n = n + 2) {
        var n2 = string0[n + parseInt(1)];
        var new_word = string0[n] + n2;
        $(".bigword").append("<span id='a" + n + "'>" + (string0[n] == " " ? "&nbsp;" : new_word) + "</span>");
        $("#a" + n + "").delay(n * 1800).queue(function(next) {
            var $this = $(this);
			setTimeout(function() {
				$this.css({"opacity": "1","left": "-2.8rem","top": "5%"});
				setTimeout(function() {
					$this.css({"top": "42%","left": "0rem"});
					setTimeout(function() {
						$this.addClass("shadow1");
						setTimeout(function() {
							$this.addClass("shadow2").removeClass("shadow1");
							setTimeout(function() {
								$this.removeClass("shadow2").addClass("lrColor");
							}, 400);
						}, 900);
					}, 10);
				}, 790);
			}, 400);
			
			/*setTimeout(function() {
				$this.addClass("shadow1").css({"opacity": "1","top": "26%","left": "-1rem",});
				setTimeout(function() {
					$this.addClass("shadow2").removeClass("shadow1").css({"top": "34%","left": "0","opacity": "1"});					
					setTimeout(function() {
						$this.removeClass("shadow2").addClass("shadow3").addClass("lrColor").css({"top": "42%","left": "1rem","opacity": "1"});
						setTimeout(function() {
							$this.removeClass("shadow3");
						}, 600);
					}, 800);
				}, 1000);
			}, 1000);*/
            next();
        });
    };
	/* SPARK TEXT */	
    function wrapWords(el) {
        $(el).filter(':not(script)').contents().each(function() {
            if (this.nodeType === Node.ELEMENT_NODE) {
                wrapWords(this);
            } else if (this.nodeType === Node.TEXT_NODE && !this.nodeValue.match(/^\s+$/m)) {
                $(this).replaceWith($.map(this.nodeValue.split(), function(w) {
                    return w.match(/^\s*$/) ? document.createTextNode(w) : $('<span>', {
                        class: 'word',
                        text: w
                    }).get();
                }));
            }
        });
    };
    $('.spark').each(function() {
        var gettxt = $(this).text();
        $(this).html(wrapWords(this));
        $(this).find('.word').each(function() {
            inkout($(this));
            $(this).addClass('initialized').css("display", "none");
        });
        $(this).append("<p style='display:none;'>" + gettxt + "</p>");
    });
    $('body').on('DOMNodeInserted', function(e) {
        if ($(e.target).is('.spark') && $(e.target).hasClass('initialized') == false) {
            setTimeout(function() {
                $(e.target).html(wrapWords(e.target));
                $(e.target).find('.word').each(function() {
                    if (!$(this).hasClass('initialized')) {
                        inkout($(this));
                        $(this).addClass('initialized');
                    }
                });
                $(e.target).addClass('initialized');
            }, 100);
        }
    });

    function css(element, property) {
		alert(window.getComputedStyle(element, null).getPropertyValue(property));
        return window.getComputedStyle(element, null).getPropertyValue(property);
    }
    function inkout(element) {
        element.parent().css('position', 'absolute');
        var startTime = new Date().getTime();
        var currentTime = startTime / 1000;
        var font = element.css('font-size') + ' ' + element.css('font-family');
        var color = element.css('color');
        var text = element.html();
        var particles = [];
        var cw = element.width()+100,
            ch = element.height();
        element.html('');
        var canvas = $('<canvas/>').attr({
                width: cw,
                height: ch
            }).css({
                display: 'inline-block',
                'vertical-align': 'text-bottom',
					'margin-left': '-10px'
            }).appendTo(element),
            context = canvas.get(0).getContext("2d");
        function drawText() {
            context.clearRect(0, 0, cw, ch);
            context.fillStyle = color;
            context.clearRect(0, 0, cw, ch);
            context.font = font;
            context.textAlign = "center";
            context.fillText(text.toUpperCase(), cw / 2, ch - (ch / 5));
        }
        $(window).resize(function() {
            element.html(text);
            font = element.css('font-size') + ' ' + element.css('font-family');
            particles = [];
            cw = element.width(),
                ch = element.height();
            element.html('');
            canvas = $('<canvas/>').attr({
                    width: cw + 12,
                    height: ch + 12
                }).css({
                    display: 'inline-block',
                    'vertical-align': 'top',
					'margin-left': '-10px'
                }).appendTo(element),
                context = canvas.get(0).getContext("2d");
            drawText();
            scramble();
        });
        drawText();
        var particle = function(x, y, visible, color) {
            this.color = 'rgba(' + color[0] + ',' + color[1] + ',' + color[2] + ',' + color[3] / 255 + ')';
            this.visible = visible;
            this.realx = x;
            this.realy = y;
            this.toplace = false;
            this.rate = Math.round(Math.random() * 12) - 6;
            this.spin = Math.round(Math.random() * 6);
            this.x = x;
            this.y = y;
            particles.push(this);
        }
        particle.prototype.draw = function() {
            var l = false;
            if (l == false) {
                this.toplace = false;
            }
            if (this.toplace == false) {
                if (this.spin == 1) {
                    this.x = this.realx + Math.floor(Math.sin(currentTime) * this.rate);
                } else if (this.spin == 0) {
                    this.y = this.realy + Math.floor(Math.cos(-currentTime) * this.rate);
                } else {
                    this.x = this.realx + Math.floor(Math.sin(-currentTime) * this.rate);
                    this.y = this.realy + Math.floor(Math.cos(currentTime) * this.rate);
                }
            } else {
                if (this.x < this.realx) {
                    this.x++;
                } else if (this.x > this.realx) {
                    this.x--;
                }
                if (this.y < this.realy) {
                    this.y++;
                } else if (this.y > this.realy) {
                    this.y--;
                }
            }
            if (this.visible == true || this.toplace == true) {
                context.fillStyle = this.color;
                context.fillRect(this.x, this.y, 1, 1);
            }
        }
        function scramble() {
            for (var y = 1; y < ch; y += 1) {
                for (var x = 0; x < cw; x++) {
                    if (context.getImageData(x, y, 1, 1).data[3] >= 1) {
                        if (Math.round(Math.random() * 3) >= 2) {
                            new particle(x, y, false, context.getImageData(x, y, 1, 1).data);
                        } else {
                            new particle(x, y, true, context.getImageData(x, y, 1, 1).data);
                        }
                    }
                }
            }
        }
        scramble();
        var requestframe = window.requestAnimationFrame || window.webkitRequestAnimationFrame || window.mozRequestAnimationFrame || window.msRequestAnimationFrame || window.oRequestAnimationFrame ||
            function(callback) {
                window.setTimeout(callback, 1000 / 60);
            };
        function loop() {
            var now = new Date().getTime();
            currentTime = (now - startTime) / 1000;
            context.clearRect(0, 0, cw, ch);
            for (var i = 0; i < particles.length; i++) {
                particles[i].draw();
            }
            requestframe(loop);
        }
        loop();
    }
	
    setTimeout(function() {
        $('.spark1 .word').fadeIn(1800);
        setTimeout(function() {
            $('.spark1 .word').fadeOut(1800);
            $('.spark1 p').fadeIn(1400);
			setTimeout(function() {
				$('.pageloader').addClass('border');
				$('.spark2 .word').fadeIn(1800);				
				$('.spark1 p').fadeOut(1400);
				setTimeout(function() {
					$('.spark2 .word').fadeOut(1800);
					$('.spark2 p').css("text-transform","uppercase").fadeIn(1400);
				}, 2400);
			}, 3600);
		}, 4000);
    }, 4000);
})();